# Chytrý ventilátor přes ESPHome

Pomocí tohoto projektu lze z jednoduchého a levného ventilátoru do zásuvky udělat chytrý ventilátor, který bude připojen k systému chytré domácnosti [Home Assistant](https://www.home-assistant.io/). Využívá se doplňku [ESPHome](https://esphome.io/), který je optimalizována a krásně si rozumí právě se systémem Home Assistant.

## Kde projekt použít a princip fungování

Tento projekt je určen na ventilátory, které regulují svojí rychlost pomocí přepínání jednotlivých vinutí statiry v motoru, čímž mění počet pólpárů a regulují tak svoje synchronní otáčky bez nutnosti změny frekvence. Jedná se o ty nejtypičtější a nejlevnější stojanové a stolní ventilátory, které se prodávají. Jakmile má ventilátor primitivní způsob přepínání rychlostí, například přepínač 0-1-2-3, mělo by být možné tento projekt na ventilátoru uskutečnit.


## Použitý hardware

### Mikrokontrolér
Jako mozek celého zařízení lze zvolit jakoukoliv verzi ESP32. ESP8266, starší mikrokontrolér od stejné firmy Espressif, má příliš málo GPIO pinů na všechny připojené periferie. V případě odebrání některých prvků by mikrokontrolér ESP8266 mohl stačit.

Já jsem si vybral ESP32 ve verzi C3 v podobě vývojové desky od firmy [WeAct](https://www.aliexpress.com/item/1005004960064227.html?). Deska je narozdíl od většiny vývojových desek velmi kompaktní. ESP32-C3 je novodobý nástupce zmíněné ESP8266. Je vybavena již dostatečným počtem GPIO pinů a dokonce Bluetooth Low Energy. Lze však zvolit jakoukoliv jinou ESP32 desku.

<img src="/images/MCU.jpg" width=400 align="center">

### Ovládání

Pro spínání jednotlivých vinutí lze použít dvě metody. Jedna metoda je využití klasického relé. Prodávají se hotové desky, které na sobě mají 3 relé a integrováné ovládání pomocí optočlenů a transistorů a signalizaci který kanál je zrovna sepnut.

Druhá možnost spínání je pomocí solid state relé, které má obrovskou výhodu v tichosti oproti normálnímu relé. Hotové SSR v sobě mají elektricky oddělení logické ovládací části a silové spínací. K spínání střídavého proudu využívají soustavu polovodičových součástek. Opět se prodávají hotové SSR moduly.

V každém případě je potřeba dávat pozor na verzi, kterou si kupujete. Některé moduly jsou spínany logickou úrovní HIGH a některé LOW.

<img src="/images/relay_module.jpg" width=400>

### Logika

Přímé připojení ovládacích pinů silového spínače přímo k mikroprocesoru je možné, avšak velmi nebezpečné. V případě, že by se seplo na ventilátoru více vinutí, mohlo by dojít k jeho poškození kvůli příliš vysokému proudu a teoreticky by se ani nemusel roztočit. Proto je mezi ovládacími vstupy a ESP umístěn integrovaný obvod. Jedná se o BCD kodér MH74141, který podle kombinace na 4 vstupech sepne právě jeden z mnoha výstupů. Na ovládání ventilátoru stačí pouze dva výstupy, avšak kvůli bootu ESP je potřeba využít tři ovládací vstupy. Při bootu ESP se na většině GPIO pinů objeví nějaká logická hodnota a při použití pouze dvou ovládacích vstupů se během bootu na moment zapne výstup i na BCD kodéru. Nezapne se více výstupu najednou, ale i krátké zatočení ventilátoru, když se točit nemá, je nežádoucí.

Kvůli špatné výstupní logice z BCD kodéru je za ním ještě integrovaný obvod HCF4011BE. Jedná se o čtyřnásobný NAND, který používám jako negátor pro obrácení logické hodnoty. Vstupy jednotlivých NAND spojím a přivedu exkluzivní výstup z BCD kodéru na jeden ze vstupů. Výstup z NAND integrovaného obvodu je již připojený na logické vstupy relé modulu.

Díky této konfiguraci není možné, aby se seplo více vinutí na motoru a došlo k poruše.


### Fyzické ovládání

Ovládání přes frontend, jako je například Home Assistant, nemusí být vždy to nejpříjemnější. Proto jsou na zařízení fyzická tlačítka, celkem 4. Slouží pouze pro vybrání rychlosti a případnému vypnutí ventilátoru.

Mimo tlačítek je na ventilátoru také IR přijímač pro možnost ovládání ventilátoru na dálku velmi jednoduše a rychle. Lze nakonfigurovat jakýkoliv ovladač. Ať už se jedná o primitivní ovladač s pár tlačítky, ovladač od staré televize, či set-top boxu. Konfigurace je velmi dobře popsána na stránkách [ESPHome](https://esphome.io/components/remote_transmitter.html#remote-setting-up-infrared).

V případě ESP32-C3 je však ještě potřeba mít do kódu přidán repozitář pomocí externího komponentu. Repozitář s návodem lze najít [zde](https://github.com/Jorre05/remote_receiver).
<p1>
    <img src="/images/IR_receiver.jpg" width="400">
    <img src="/images/remote.jpg" height="300">
</p1>

### Zobrazování informací

Další periferie připojená k ESP je 0,96" OLED displej připojený přes I<sup>2</sup>C sběrnici. ESPHome má podporu pro různé displeje včetně dalších verzí OLED displejů (1,3", 0,91",...), ale také LCD displeje a mnoho dalších. Na displeji je zobrazená aktuální rychlost ventilátoru společně s časem, který je sbírán pomocí sntp z internetu. Je také možnost sbírát čas pomocí RTC modulu, nebo ze serveru, na kterém běží Home Assistant.

Jednou za určitý čas (přednastaveno na 10 sekund) se změní na displeji stránka ukazující teplotu a vlhkost, kterou si ESP stahuje z Home Assistanta.

<p1>
    <img src="/images/oled_display.jpg" width=500>
    <img src="/images/TandH.jpg" width=500>
</p1>

<p1>
    <img src="/images/speed0.jpg" width=250>
    <img src="/images/speed1.jpg" width=250>
    <img src="/images/speed2.jpg" width=250>
    <img src="/images/speed3.jpg" width=250>
</p1>

## Instalace
Zatím je nutné pro instalaci do Vašeho projektu buď většinu částí z yaml souboru zkopírovat do Vašeho projektu a upravit přiřazení GPIO pinů, displej, apod. Nebo yaml soubor z tohoto repozitáře stáhnout a upravit k Vašim potřebám.

## Budoucí vylepšení
- [ ] Přidání časovače
- [ ] Vytvoření DPS
- [ ] Vytvořeni varianty, která bude mít místo 0-1-2-3 jednoduché přepínání na další rychlost a tlačítko vypnout.
- [ ] Varianta s RF ovladačem
- [ ] Vytvoření vlastního IR/RF ovladače (ATTiny55/13A?)
- [ ] Vytvoření [external component](https://esphome.io/components/external_components.html) pro jednodušší konfiguraci a instalaci do projektu
- [ ] Vytvoření uchycení do alespoň jednoho specifického ventilátoru
- [ ] Vyzkoušení a vytvoření různých verzí pro různé displeje -> parametr volby v externím komponentu
- [ ] Přizpůsobení ESP k provozu nonstop: snížení spotřeby a síťového provozu
- [ ] Vytvoření přepínání rychlostí pomocí tlačítka na ovladači i u verze s tlačítky na změnu na určitou rychlost
